// console.log('Hello, World');

// Item 3
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {
	console.log(data)
}) 



/*//Item 4
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {

	let title = data

	let onlyTitle = title.map(function(title){
		return data.title
	})

	console.log(title)
}) */



// Item 5 & 6
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {
	console.log(data)
	console.log(`The item ${data.title} has a status of false script`)
}) 



// Item 7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		userId:1,
		title:"Created a To Do List Item",
		completed:false
	})
})
.then(response => response.json())
.then(json => console.log(json))



// Item 8 & 9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:"Updated To Do List Item",
		description:"To update the my to do list with a different data structure",
		status:"Pending",
		dateCompleted:"Pending",
		userId:1
	})
})
.then(response => response.json())
.then(json => console.log(json))



// Item 10 & 11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		status:"Complete",
		dateCompleted:"07/09/21"
		
	})
})
.then(response => response.json())
.then(json => console.log(json))



// Item 12
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))